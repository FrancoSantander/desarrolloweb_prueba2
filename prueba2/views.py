from django.http import HttpResponse
from django.template.loader import get_template
import math

class Casa(object):
    def __init__(self,medida,casa):
        self.medida=medida 
        self.casa=casa
        

#ejemplo de una vista
def clase(request):
    objeto = Casa("100","metros")
    casas = ["100 metros","200 metros","300 metros","400 metros"]   
    archivo_externo = get_template("pormetraje.html")
    documento = archivo_externo.render({"medida_casa":objeto.medida,"casa_casa":objeto.casa,"casas_":casas})
    return HttpResponse(documento)


def bienvenida(request):
    texto="Casas disponibles"
    return HttpResponse(texto)

def salida(request):
    texto="no hay mas casas"
    formato="""
    <h1> %s </h1>
    """ % texto
    return HttpResponse(formato)

def exponenciar(request,valor):
    resultado = math.pow(valor,2)
    texto="""
    las casas son%s <br>
    la medida es%s <br>
    """%(valor,resultado)
    return HttpResponse(texto)


